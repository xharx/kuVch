# 金蝶KIS旗舰版财务凭证同步WebAPI接口标准版

------

- [接口简介](#接口简介)
- [接口说明](#接口说明)
- [登录](#登录)
- [退出登录](#退出登录)
- [会计科目数量](#会计科目数量)
- [会计科目](#会计科目)
- [凭证字号](#凭证字号)
- [币别](#币别)
- [查看核算项目类型](#查看核算项目类型)
- [基础资料数量](#基础资料数量)
- [查看核算项目](#查看核算项目)
- [同步客户](#同步客户)
- [同步供应商](#同步供应商)
- [同步部门](#同步部门)
- [同步职员](#同步职员)
- [同步仓库](#同步仓库)
- [同步自定义核算项目](#同步自定义核算项目)
- [同步凭证](#同步凭证)
- [批量同步凭证](#批量同步凭证)
- [删除凭证](#删除凭证)
- [C#调用示例](#C#调用示例)
- [VB.net调用示例](#VB.net调用示例)

## 接口简介

本文档为金蝶KIS旗舰版财务凭证同步WebAPI接口技术文档，本接口可供任何第三方业务系统调用，以生成金蝶KIS旗舰版财务记账凭证。本接口支持金蝶KIS旗舰版全部版本。在可预期的未来，也将继续支持未来新发布的私有部署版本或自购云主机安装部署方式。本接口有偿提供给伙伴使用，详情请加VX 13525178050进行了解。
1. 调用登录接口，通过AcctID区分不同帐套；
2. 获得用户token（每次登录会**重新生成**，原token失效）；
3. 调用凭证同步接口（按格式构造参数）；
4. 同步完毕后，调用退出接口以使token失效。若不调用退出接口，则token在重新登录前一直有效。<br/>

- **AcctID**：金蝶KIS旗舰版帐套ID，用此ID来连接不同的帐套，数据来自select FAcctID from AcctCtl_KEE..t_ad_kdAccount_gl。
- **Token**:登录成功后返回给调用端的用户身份认证字符串，访问其他所有接口均需提供。
- **接口地址**：在IIS里发布的地址。例如http://192.168.1.106:90/api 。
- **基础资料分页**：金蝶KIS旗舰版系统的基础资料会较多，因此均提供一次性读取接口和分页读取接口。
- **部署**：本接口可部署在金蝶KIS旗舰版服务器上，也可部署在与金蝶KIS旗舰版服务器同一局域网内的其他计算机系统上。部署接口的计算机推荐用Windows Server版操作系统。
- **跨域**：支持跨域调用。
- **其他接口**：[金蝶KIS专业版凭证接口（WebAPI）](https://gitee.com/xharx/vch)，[金蝶K/3 WISE凭证接口（WebAPI）](https://gitee.com/xharx/k3vch)。


## 接口说明

所有接口以RESTful的方式访问，参数及返回值均为采用UTF-8编码的标准JSON格式字符串。

接口对凭证的新增、删除操作，需相应的用户权限。请以管理员身份登录金蝶KIS旗舰版，在`系统设置-用户管理-用户管理-用户管理`里进行权限设置。

## 登录

**简要描述：** 登录到金蝶KIS旗舰版

**请求 URL：** `/login`

**请求方式：** POST

**请求参数：**

```java
{
    "User":                        //用户登录信息段
    {
        "Name":"Administrator",    //用户名称，与KIS旗舰版登录用户名一致
        "Password":"191101"        //用户登录密码，与KIS旗舰版登录用户的密码一致
    },
    "AcctID":1008                  //帐套ID，数据来自select FAcctID from AcctCtl_KEE..t_ad_kdAccount_gl
}
```

**返回串及字段说明**

```java
{
    "Token": "978a2e1e19ffc5d55de089f537938d20",   //Token值，访问其他接口均需提供此值
    "Result": 16394,                               //登录成功，返回用户内码，均大于等于16394；登录失败，返回小于1的值
    "Message": "登录成功"                           //登录结果提示信息 
}
```

## 退出登录

**简要描述：** 功能接口调用完毕后，请调用此接口退出，以使用户Token失效。否则，Token将一直有效。直到用户下次登录时被覆盖。

**请求 URL：** `/logout`

**请求方式：** POST

**请求参数：**

```java
{
    "Token":"0949860757611aeeee011f58e5600886"    //Token值
}
```

**返回串及字段说明**

```java
{
    "Result": 1,                          //接口执行结果：1成功，其他值失败
    "Message": "Token删除成功"             //执行结果的具体文字描述
}
```

## 会计科目数量

**简要描述：** 返回会计科目的数量。用于当分页调取数据时，可以根据数量合理的进行分页。

**请求 URL：** `/count`

**请求方式：** POST

**请求参数：**

```java
{
    "Token":"978a2e1e19ffc5d55de089f537938d20"
}
```

**返回串及字段说明**

```java
{
    "Result": 21958,       //会计科目数量
    "Message": ""          
}
```
## 会计科目

**简要描述：** 获取金蝶KIS旗舰版指定帐套的会计科目列表。如需分页返回数据，请在URL参数里指定page和count。

**请求 URL：** `/accounts`

**请求方式：** POST

**Url请求参数：**
| 参数 | 含义 | 必选 | 
| :-----| :----- | :----- | 
| page | 第几页 | 否，可省略 | 
| count | 每页数据数量 | 否，可省略 | 

*page和count参数必须同时出现或同时省略，不得只出现一个。*  
*page和count参数省略时，返回全部数据。*  
*例如，要返回第5页的数据，每页返回100条。则请求URL为`/accounts?page=5&count=100`*

**Body请求参数：**

```java
{
    "Token":"978a2e1e19ffc5d55de089f537938d20"
}
```

**返回串及字段说明**

```java
{
    "Page": 8,                               //当前页数，如调用接口时未指定此参数，则为1
    "Count": 2,                              //本页记录数，如调用接口时未指定此参数，则为实际返回的科目数
    "Accounts": [                            //会计科目列表
        {
            "Id": 1020,                      //科目内码
            "Code": "1121",                  //科目编码
            "Name": "应收票据",               //科目名称
            "FullName": "应收票据",           //科目完整名称
            "Group": "流动资产",              //科目类别
            "Level": 1,                      //科目级次
            "IsDetail": true,                //是否明细科目：true是明细科目，false非明细科目
            "ParentId": 0,                   //上级科目内码。0代表该科目为一级科目
            "DC": "借",                      //科目余额方向
            "Currency": "人民币",             //币别
            "IsQuantity": false,             //科目是否进行数量金额核算：true进行数量金额核算；false不进行数量金额核算
            "Unit": "",                      //如进行数量金额核算，科目指定的计量单位；不进行数量金额核算的科目，此字段为空
            "IsCash": false,                 //是否现金科目
            "IsBank": false,                 //是否银行科目
            "Items": [],                     //科目下挂核算项目，科目可能挂多个核算项目，因此此字段为列表
            "IsForbidden": false             //是否禁用
        },
        {
            "Id": 1021,
            "Code": "1122",
            "Name": "应收账款",
            "FullName": "应收账款",
            "Group": "流动资产",
            "Level": 1,
            "IsDetail": true,
            "ParentId": 0,
            "DC": "借",
            "Currency": "全部币别",
            "IsQuantity": false,
            "Unit": "",
            "IsCash": false,
            "IsBank": false,
            "Items": [                       //科目下挂核算项目，科目可能挂多个核算项目，因此此字段为列表
                {
                    "Id": 1,                 //核算项目类型内码
                    "Code": "001",           //核算项目类型编码
                    "Name": "客户"           //核算项目类型名称
                }
            ],
            "IsForbidden": false
        }
    ],
    "Result": 1,
    "Message": "成功"
}
```

## 凭证字号

**简要描述：** 返回所有凭证字及对应的最大凭证号

**请求 URL：** `/words`

**请求方式：** POST

**请求参数：**

```java
{
    "Token":"978a2e1e19ffc5d55de089f537938d20"
}
```

**返回串及字段说明**

```java
{
    "Result": 1,                       //1：成功；小于1的值，失败，需检查Message字段了解失败原因
    "Message": "凭证字号获取成功",
    "Year": 2021,                      //账套当前会计年度
    "Period": 2,                       //账套当前会计期间
    "Numbers": [                       //账套里可能有多个凭证字，因此为列表字段
        {
            "Id": 1,                   //凭证字内码
            "Name": "记",              //凭证字
            "Number": 166              //该凭证字对应的最大凭证号+1。例如系统记字对应的最大凭证号为165，则此处返回166。
        }
    ]
}
```
## 币别

**简要描述：** 返回指定帐套的币别信息

**请求 URL：** `/currency`

**请求方式：** POST

**请求参数：**

```java
{
    "Token":"978a2e1e19ffc5d55de089f537938d20"
}
```

**返回串及字段说明**

```java
{
    "Data": [                  //账套里可能存在多个币别，因此为列表字段
        {
            "Code": "RMB",     //币别编码  
            "Name": "人民币"    //币别名称
        },
        {
            "Code": "USD",
            "Name": "美元"
        }
    ],
    "Result": 1,                //1：成功；小于1的值，失败，需检查Message字段了解失败原因 
    "Message": ""
}
```
## 查看核算项目类型

**简要描述：** 有些会计科目下挂一个或多个核算项目，凭证接口在提交数据时，需要提供核算项目的内码，本接口是核算项目接口的前置接口。需先通过本接口获取核算项目类型的内码，然后提交核算项目类型内码至查看核算项目接口。

**请求 URL：** `/class`

**请求方式：** POST

**请求参数：**

```java
{
    "Token":"978a2e1e19ffc5d55de089f537938d20"
}
```

**返回串及字段说明**

```java
{
    "Data": [
        {
            "Id": 1,              //核算项目类型内码
            "Code": "001",        //核算项目类型编码
            "Name": "客户"        //核算项目类型名称
        },
        {
            "Id": 2,
            "Code": "002",
            "Name": "部门"
        },
        {
            "Id": 3,
            "Code": "003",
            "Name": "职员"
        },
        {
            "Id": 4,
            "Code": "004",
            "Name": "物料"
        },
        {
            "Id": 5,
            "Code": "005",
            "Name": "仓库"
        },
        {
            "Id": 6,
            "Code": "006",
            "Name": "备注"
        },
        {
            "Id": 7,
            "Code": "007",
            "Name": "计量单位"
        },
        {
            "Id": 8,
            "Code": "008",
            "Name": "供应商"
        }
    ],
    "Result": 1,                  //1：成功；小于1的值，失败，需检查Message字段了解失败原因 
    "Message": ""
}
```
## 基础资料数量

**简要描述：** 返回基础资料的数量。用于当分页调取数据时，可以根据数量合理的进行分页。根据**查看核算项目类型**接口获取的核算项目类型内码，获取对应核算项目的数量。

**请求 URL：** `/count?class=核算项目类型内码`，例如`/count?class=4`为查看物料的数量。

**请求方式：** POST

**请求参数：**

```java
{
    "Token":"978a2e1e19ffc5d55de089f537938d20"
}
```

**返回串及字段说明**

```java
{
    "Result": 23581,    //指定类型的核算项目的数量
    "Message": ""
}
```
## 查看核算项目

**简要描述：** 根据**查看核算项目类型**接口获得核算项目类型内码后，提交具体内码可以查看该类别的核算项目，按编码排序，不显示已禁用项目。例如，要查看客户信息，就在参数里提交1。依次类推。如需分页返回数据，请在URL参数里指定page和count。

**请求 URL：** `/item?class=核算项目类型内码`，如要查看客户，则为`/item?class=1`

**请求方式：** POST

**Url请求参数：**
| 参数 | 含义 | 必选 | 
| :-----| :----- | :----- | 
| page | 第几页 | 否，可省略 | 
| count | 每页数据数量 | 否，可省略 | 

*page和count参数必须同时出现或同时省略，不得只出现一个。*  
*page和count参数省略时，返回全部数据。*  
*例如，要返回客户的第1页的数据，每页返回50条。则请求URL为`/item?class=1&page=1&count=50`*

**Body请求参数：**

```java
{
    "Token":"978a2e1e19ffc5d55de089f537938d20"
}
```

**返回串及字段说明**

```java
{
    "Data": [
        {
            "Id": 363,                             //核算项目内码
            "Code": "1001",                        //核算项目编码
            "Name": "凡尔赛电脑公司"                //核算项目名称
        },
        {
            "Id": 364,
            "Code": "1002",
            "Name": "奥德赛电子有限公司"
        }
    ],
    "Result": 1,                                    //1：成功；小于1的值，失败，需检查Message字段了解失败原因 
    "Message": ""
}

```
## 同步客户

**简要描述：** 在金蝶KIS旗舰版指定账套里创建一条**仅包含编码和名称**的客户信息，用户需具备新增客户的权限；若编码已存在，则更新编码对应的客户名称，用户需具备修改客户的权限。

**请求 URL：** `/customer`

**请求方式：** POST

**请求参数：**

```java
{
    "Data":{
        "Code":"0000",                         //客户编码
        "Name":"太平洋公司"                     //客户名称
    },
    "Token":"46e87f1c36f41e32c853b3ea71f360a1"
}
```

**返回串及字段说明**

```java
{
    "Data": {
        "Id": 8702,                           //客户内码
        "Code": "0000",                       //客户编码
        "Name": "太平洋公司"                   //客户名称
    },
    "Result": 1,                              //1：成功；小于1的值，失败，Message字段为失败的具体描述
    "Message": ""
}
```
## 同步供应商

**简要描述：** 在金蝶KIS旗舰版指定账套里创建一条**仅包含编码和名称**的供应商信息，用户需具备新增供应商的权限；若编码已存在，则更新编码对应的供应商名称，用户需具备修改供应商的权限。

**请求 URL：** `/supplier`

**请求方式：** POST

**请求参数：**

```java
{
    "Data":{
        "Code":"0000",                         //供应商编码
        "Name":"正能公司"                       //供应商名称
    },
    "Token":"46e87f1c36f41e32c853b3ea71f360a1"
}
```

**返回串及字段说明**

```java
{
    "Data": {
        "Id": 8703,                           //供应商内码
        "Code": "0000",                       //供应商编码
        "Name": "正能公司"                     //供应商名称
    },
    "Result": 1,                              //1：成功；小于1的值，失败，Message字段为失败的具体描述
    "Message": ""
}
```
## 同步部门

**简要描述：** 在金蝶KIS旗舰版指定账套里创建一条**仅包含编码和名称**的部门信息，用户需具备新增部门的权限；若编码已存在，则更新编码对应的部门名称，用户需具备修改部门的权限。

**请求 URL：** `/department`

**请求方式：** POST

**请求参数：**

```java
{
    "Data":{
        "Code":"1000",                         //部门编码
        "Name":"人力资源部"                     //部门名称
    },
    "Token":"46e87f1c36f41e32c853b3ea71f360a1"
}
```

**返回串及字段说明**

```java
{
    "Data": {
        "Id": 8704,                           //部门内码
        "Code": "1000",                       //部门编码
        "Name": "人力资源部"                   //部门名称
    },
    "Result": 1,                              //1：成功；小于1的值，失败，Message字段为失败的具体描述
    "Message": ""
}
```
## 同步职员

**简要描述：** 在金蝶KIS旗舰版指定账套里创建一条**仅包含编码和名称**的职员信息，用户需具备新增职员的权限；若编码已存在，则更新编码对应的职员名称，用户需具备修改职员的权限。

**请求 URL：** `/employee`

**请求方式：** POST

**请求参数：**

```java
{
    "Data":{
        "Code":"1000",                         //职员编码
        "Name":"张思睿"                         //职员名称
    },
    "Token":"46e87f1c36f41e32c853b3ea71f360a1"
}
```

**返回串及字段说明**

```java
{
    "Data": {
        "Id": 8705,                           //职员内码
        "Code": "1000",                       //职员编码
        "Name": "张思睿"                      //职员名称
    },
    "Result": 1,                              //1：成功；小于1的值，失败，Message字段为失败的具体描述
    "Message": ""
}
```
## 同步仓库

**简要描述：** 在金蝶KIS旗舰版指定账套里创建一条**仅包含编码和名称**的仓库信息，用户需具备新增仓库的权限；若编码已存在，则更新编码对应的仓库名称，用户需具备修改仓库的权限。

**请求 URL：** `/warehouse`

**请求方式：** POST

**请求参数：**

```java
{
    "Data":{
        "Code":"10",                           //仓库编码
        "Name":"原料仓"                         //仓库名称
    },
    "Token":"46e87f1c36f41e32c853b3ea71f360a1"
}
```

**返回串及字段说明**

```java
{
    "Data": {
        "Id": 8706,                           //仓库内码
        "Code": "10",                         //仓库编码
        "Name": "原料仓"                      //仓库名称
    },
    "Result": 1,                              //1：成功；小于1的值，失败，Message字段为失败的具体描述
    "Message": ""
}
```
## 同步自定义核算项目

**简要描述：** 在金蝶KIS旗舰版指定账套里创建一条**仅包含编码和名称**的自定义核算项目信息，用户需具备新增相应的自定义核算项目的权限；若编码已存在，则更新编码对应的自定义核算项目名称，用户需具备修改相应自定义核算项目的权限。

**请求 URL：** `/sd?code=自定义核算项目类别编码`，例如自定义核算项目[车辆]，其类别编码为C001，则同步车辆的接口地址为`/sd?code=C001`

**Url请求参数：**
| 参数 | 含义 | 必选 | 
| :-----| :----- | :----- | 
| code | 自定义核算项目类别编码 | 必录 | 

**请求方式：** POST

**请求参数：**

```java
{
    "Data":{
        "Code":"1999",                            //自定义核算项目编码
        "Name":"豫A12345"                         //自定义核算项目名称
    },
    "Token":"46e87f1c36f41e32c853b3ea71f360a1"
}
```

**返回串及字段说明**

```java
{
    "Data": {
        "Id": 8707,                           //自定义核算项目内码
        "Code": "1999",                       //自定义核算项目编码
        "Name": "豫A12345"                    //自定义核算项目名称
    },
    "Result": 1,                              //1：成功；小于1的值，失败，Message字段为失败的具体描述
    "Message": ""
}
```
## 同步凭证

**简要描述：** 在金蝶KIS旗舰版指定帐套里创建一张记账凭证，用户需具备新增凭证权限。

**请求 URL：** `/add`

**请求方式：** POST

**请求参数：**

```java
{
    "Data": {
        "Word": "记",                                      //凭证字，必须提供
        "Number": 0,                                       //凭证号，任意提供，接口会自动按流水号递增凭证号                    
        "Attachments": 3,                                  //附件数
        "Date": "2021-02-28",                              //凭证日期，即同步凭证时的日期
        "BussnessDate": "2021-02-10",                      //业务日期，指业务发生时的日期
        "Reference": "webapi创建凭证",                      //参考信息
        "Entries": [                                       //凭证分录，至少两条且借贷平衡
            {
                "Explanation": "购进材料一批",              //摘要
                "AcctNumber": "1404.01",                   //科目代码
                "DC": true,                                //借贷方向：true借方；false贷方
                "Currency": "RMB",                         //币别编码
                "Qty": 80.0,                               //分录数量，科目进行数量金额核算的情况下需提供；未进行数量金额核算的科目，可省略此字段
                "Price": 7,                                //分录单价，科目进行数量金额核算的情况下需提供；未进行数量金额核算的科目，可省略此字段
                "Amount": 560.0,                           //分录本位币金额
                "Items": [279,308,5109]                    //核算项目内码列表，该字段为整形数组列表；未下挂核算项目的科目，可省略此字段。参见第2条分录
            },
            {
                "Explanation": "以美元账户转账支付460",      //摘要
                "AcctNumber": "1002.02",                   //科目代码
                "DC": false,                               //借贷方向：true借方；false贷方
                "Currency": "USD",                         //币别编码
                "ExchangeRate":6.67,                       //汇率，如分录为外币，则需提供汇率值；未进行外币核算的科目，可省略此字段
                "Amount": 460.0                            //分录本位币金额，需将外币换算为人民币
            },
            {
                "Explanation": "以现金支付100",             //摘要
                "AcctNumber": "1001",                      //科目代码
                "DC": false,                               //借贷方向：true借方；false贷方
                "Currency": "RMB",                         //币别编码
                "Amount": 100.0                            //分录本位币金额
            }
        ]
    },
    "Token": "978a2e1e19ffc5d55de089f537938d20"
}

```

**返回串及字段说明**

```java
{
    "Result": 1,            //1：成功；小于1的值，失败，需检查Message字段了解失败原因 
    "Message": "",
    "Year": 2021,           //同步的凭证所在会计年度
    "Period": 2,            //同步的凭证所在会计期间   
    "Voucher": {
        "Word": "记",       //凭证字
        "Number": 166       //凭证号
    }
}
```

## 批量同步凭证

**简要描述：** 在金蝶KIS旗舰版指定帐套里同步一批记账凭证，用户需具备新增凭证权限。批量同步存在部分凭证创建成功、部分凭证创建失败的情况，因此需检查返回值的Vouchers字段。

**请求 URL：** `/adds`

**请求方式：** POST

**请求参数：**

```java
{
    "Data": [
        {
        "Word": "记",                                      //凭证字，必须提供
        "Number": 0,                                       //凭证号，任意提供，接口会自动按流水号递增凭证号                    
        "Attachments": 3,                                  //附件数
        "Date": "2021-02-28",                              //凭证日期，即同步凭证时的日期
        "BussnessDate": "2021-02-10",                      //业务日期，指业务发生时的日期
        "Reference": "webapi创建凭证",                      //参考信息
        "Entries": [                                       //凭证分录，至少两条且借贷平衡
            {
                "Explanation": "购进材料一批",              //摘要
                "AcctNumber": "1404.01",                   //科目代码
                "DC": true,                                //借贷方向：true借方；false贷方
                "Currency": "RMB",                         //币别编码
                "Qty": 80.0,                               //分录数量，科目进行数量金额核算的情况下需提供；未进行数量金额核算的科目，可省略此字段
                "Price": 7,                                //分录单价，科目进行数量金额核算的情况下需提供；未进行数量金额核算的科目，可省略此字段
                "Amount": 560.0,                           //分录本位币金额
                "Items": [279,308,5109]                    //核算项目内码列表，该字段为整形数组列表；未下挂核算项目的科目，可省略此字段。参见第2条分录
            },
            {
                "Explanation": "以美元账户转账支付460",      //摘要
                "AcctNumber": "1002.02",                   //科目代码
                "DC": false,                               //借贷方向：true借方；false贷方
                "Currency": "USD",                         //币别编码
                "ExchangeRate":6.67,                       //汇率，如分录为外币，则需提供汇率值；未进行外币核算的科目，可省略此字段
                "Amount": 460.0                            //分录本位币金额，需将外币换算为人民币
            },
            {
                "Explanation": "以现金支付100",             //摘要
                "AcctNumber": "1001",                      //科目代码
                "DC": false,                               //借贷方向：true借方；false贷方
                "Currency": "RMB",                         //币别编码
                "Amount": 100.0                            //分录本位币金额
            }
        ]
    },
    {
            "Word": "记",
            "Number": 0,                  
            "Attachments": 1,
            "Date": "2021-02-28",
            "BussnessDate": "2021-02-11",
            "Reference": "",
            "Entries": [
                {
                    "Explanation": "取现",
                    "AcctNumber": "1001",
                    "DC": true,
                    "Currency": "RMB",
                    "Amount": 2000.0
                },
                {
                    "Explanation": "取现",
                    "AcctNumber": "1002.01",
                    "DC": false,
                    "Currency": "RMB",
                    "Amount": 2000.0
                }
            ]
        }
    ],
    "Token": "978a2e1e19ffc5d55de089f537938d20"
}

```

**返回串及字段说明**

```java
{
    "Result":1,                                           //1：成功；0：部分成功，存在保存失败的凭证；<0，全部失败
    "Message":"凭证全部保存成功",                          //接口执行情况信息，是对Result字段的具体解释 
    "Vouchers":[                                          //凭证同步结果列表
        {
            "Result":1,                                   //1：保存成功；<1：保存失败
            "Message":"",                                 //如果保存失败，则此处记录具体失败的原因
            "Year":2021,                                  //凭证所属会计年度
            "Period":2,                                   //凭证所属会计期间
            "Word":"记",                                  //凭证字
            "Number":271                                  //凭证号
        },
        {"Result":1,"Message":"","Year":2021,"Period":2,"Word":"记","Number":272}
    ]
}
```

## 删除凭证

**简要描述：** 在金蝶KIS旗舰版指定帐套删除指定会计年度、会计期间的一批凭证，用户需具备凭证删除权限和（或）删除他人凭证的权限。

**请求 URL：** `/delete`

**请求方式：** POST

**请求参数：**

```java
{
    "Year":2021,                          //会计年度
    "Period":2,                           //会计期间
    "Vouchers":[                          //要删除的凭证列表
        {"Word":"记","Number":166},       //凭证字，凭证号
        {"Word":"记","Number":163}
    ],
    "Token":"978a2e1e19ffc5d55de089f537938d20"
}

```

**返回串及字段说明**

```java
{
    "Result": 0,                                              //1：执行成功；0：执行成功，但因为已审核、过账或权限原因，导致有未删除的凭证；小于0，失败
    "Message": "",
    "Year": 2021,                                             //会计年度
    "Period": 2,                                              //会计期间
    "Vouchers": [                                             //删除结果列表
        {
            "Result": 1,
            "Message": "凭证[2021年2期记字第166号]删除成功",
            "Word": "记",
            "Number": 166
        },
        {
            "Result": -2,
            "Message": "凭证[2021年2期记字第163号]无删除他人凭证权限",
            "Word": "记",
            "Number": 163
        }
    ]
}
```

## 查看接口状态

**简要描述：** 查看接口状态。

**请求 URL：** `/AppStatus`

**请求方式：** GET

**返回串及字段说明**

```java
{
    "AppStatus":"演示版",                               //演示版还是正式版
    "DemoStatus":"仅能同步凭证到结账未超过[2]期的账套",   //演示版限制条件
    "ConnectionStatus":"数据连接正常",                  //数据连接状态
    "Serial":"",                                       //旗舰版序列号
    "KisStatus":"旗舰版系统管理未运行"                   //旗舰版系统管理运行状态
}
```

## C#调用示例

```C#
using System;
using System.Net;

namespace VisitWebAPI_C
{
    class Program
    {
        static void Main(string[] args)
        {
            //以访问登录接口为例
            string result_data;
            string url = "http://192.168.1.102:90/api/login";
            //含义：用户Manager登录，该用户密码为空，要登录的帐套内码为24
            //具体提交参数格式请阅读接口技术手册
            string param = "{\"User\":{\"Name\":\"Manager\",\"Password\":\"\"},\"AcctID\":1008}";
            WebClient client = new WebClient();
            client.Headers.Add("Content-Type", "application/json;charset=utf-8");
            //给访问webapi的客户端一个标识
            //从而方便从日志中甄别出非法访问客户端
            //也可以不要此句
            client.Headers.Add("User-Agent", "QenghooRuntime/1.0");
            result_data = client.UploadString(url, "POST", param);
            Console.WriteLine(result_data);
            Console.ReadLine();
        }
    }
}

```

## VB.net调用示例
```vb
Imports System.Net
Module Module1

    Sub Main()
        '以访问登录接口为例
        Dim result_data As String
        '此处将192.168.1.102替换为服务器实际地址
        Dim url As String = "http://192.168.1.102:90/api/login"
        '含义：用户Manager登录，该用户密码为空，要登录的帐套内码为24
        '具体提交参数格式请阅读本技术手册
        Dim param As String = "{""User"":{""Name"":""Manager"",""Password"":""""},""AcctID"":24}"
        Dim client As New WebClient
        client.Headers.Add("Content-Type", "application/json;charset=utf-8")
        '给访问webapi的客户端一个标识
        '从而方便从日志中甄别出非法访问客户端
        '也可以不要此句
        client.Headers.Add("User-Agent", "QenghooRuntime/1.0")
        result_data = client.UploadString(url, "POST", param)
        Console.WriteLine(result_data)
        Console.ReadLine()
    End Sub

End Module
```